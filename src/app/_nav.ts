import { INavData } from '@coreui/angular';

export const availableSports = [
  {
    name: 'Football',
    url: '/user',
    icon: 'icon-puzzle',
    children: [
      {
        name: ' Beginner',
        url: '/user/sport/football/beginner',
        icon: 'icon-ball'
      },
      {
        name: ' Regular',
        url: '/user/sport/football/regular',
        icon: 'icon-ball'
      },
      {
        name: ' Professional',
        url: '/user/sport/football/professional',
        icon: 'icon-ball'
      }
    ]
  },
  {
    name: 'Basketball',
    url: '/user',
    icon: 'icon-puzzle',
    children: [
      {
        name: ' Beginner',
        url: '/user/sport/basketball/beginner',
        icon: 'icon-ball'
      },
      {
        name: ' Regular',
        url: '/user/sport/basketball/regular',
        icon: 'icon-ball'
      },
      {
        name: ' Professional',
        url: '/user/sport/basketball/professional',
        icon: 'icon-ball'
      }
    ]
  },
  {
    name: 'Biliard',
    url: '/user',
    icon: 'icon-puzzle',
    children: [
      {
        name: ' Beginner',
        url: '/user/sport/biliard/beginner',
        icon: 'icon-ball'
      },
      {
        name: ' Regular',
        url: '/user/sport/biliard/regular',
        icon: 'icon-ball'
      },
      {
        name: ' Professional',
        url: '/user/sport/biliard/professional',
        icon: 'icon-ball'
      }
    ]
  },
  {
    name: 'Tennis',
    url: '/user',
    icon: 'icon-puzzle',
    children: [
      {
        name: ' Beginner',
        url: '/user/sport/tennis/beginner',
        icon: 'icon-ball'
      },
      {
        name: ' Regular',
        url: '/user/sport/tennis/regular',
        icon: 'icon-ball'
      },
      {
        name: ' Professional',
        url: '/user/sport/tennis/professional',
        icon: 'icon-ball'
      }
    ]
  },
  {
    name: 'Darts',
    url: '/user',
    icon: 'icon-puzzle',
    children: [
      {
        name: ' Beginner',
        url: '/user/sport/darts/beginner',
        icon: 'icon-ball'
      },
      {
        name: ' Regular',
        url: '/user/sport/darts/regular',
        icon: 'icon-ball'
      },
      {
        name: ' Professional',
        url: '/user/sport/darts/professional',
        icon: 'icon-ball'
      }
    ]
  }
];

export const navItems: INavData[] = [
  {
    name: 'My profile',
    url: '/user/profile',
    icon: 'icon-user',
    /*badge: {
      variant: 'info',
      text: 'NEW'
    }*/
  },
  {
    title: true,
    name: 'Sports'
  }, ...availableSports
];
