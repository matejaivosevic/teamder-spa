import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatchService } from 'src/app/services/match.service';

@Component({
  selector: 'app-create-match-form',
  templateUrl: './create-match-form.component.html',
  styleUrls: ['./create-match-form.component.scss']
})
export class CreateMatchFormComponent implements OnInit {

  public sport;
  public category;

  public matchForm: FormGroup;

  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private matchService: MatchService,
    private router: Router) { 
      this.matchForm = this.createMatchForm();
    }

  ngOnInit() {
    this.sport = this.route.snapshot.params.sport;
    var cat = this.route.snapshot.params.mode;
    this.category = cat.charAt(0).toUpperCase() + cat.slice(1);
  }

  public submitForm() {
    this.matchForm.patchValue({'category':this.category, 'sport':this.sport});
    this.matchService.createMatch(this.matchForm.value).subscribe(res => {
      this.router.navigateByUrl(`/user/sport/${this.sport}/${this.category.toLowerCase()}`);
    }, err => {
      console.log(err);
    });
  }

  private createMatchForm() {
    return this.formBuilder.group({
      'name': ['', Validators.compose([Validators.required])],
      'place': ['', Validators.compose([Validators.required])],
      'city': [JSON.parse(localStorage.getItem('user')).city],
      'category': [this.category],
      'requiredNumberOfPlayers': ['', Validators.compose([Validators.required])],
      'availableNumberOfPlayers': ['', Validators.compose([Validators.required])],
      'startingAt' : ['', Validators.compose([Validators.required])],
      'endingAt': [''],
      'description' : [''],
      'isPublic': [false],
      'sport' : [''],
      'matchOwnerID' : [JSON.parse(localStorage.getItem('user')).userID]
    });
  }
  
  public get startingAt() {
    return this.matchForm.get('startingAt');
  }
  
  public get endingAt() {
    return this.matchForm.get('endingAt');
  }
}
