import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatchService } from 'src/app/services/match.service';
import { SignalRService } from 'src/app/services/signal-r.service';
import { parse } from 'querystring';
import { availableSports} from "./../../_nav";

@Component({
  selector: 'app-sport',
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.scss']
})
export class SportComponent implements OnInit {

  public sport;
  public category;
  public userId;
  public city;

  public hideButton = false;
  public hideJoinButton = false;

  public refresh: boolean = false;

  public dataSet = [];
  public refreshDataSet = [];

  public appliedMatches = [];

  constructor(private route: ActivatedRoute,
    private matchService: MatchService,
    private router: Router,
    private signalRService: SignalRService) {
      this.userId = JSON.parse(localStorage.getItem('user')).userID;
      this.city = JSON.parse(localStorage.getItem('user')).city;
    }

  ngOnInit() {
    this.signalRService.startConnection();
    this.signalRService.addQueueIncrementDataListener();
    this.route.params.subscribe(res => {
      const {children: availableCategories} = availableSports[0];
      const {url: defaultUrl} = availableCategories[0];
      const {sport: searchedSport} = this.route.snapshot.params;

      const isSportExists = availableSports.find(availableSport => availableSport.name.toLowerCase() === searchedSport);
      if (!isSportExists) {
       this.router.navigateByUrl(defaultUrl);
      }
      else {
        const {mode: searchedCategory} = this.route.snapshot.params;
        const isCategoryExists = availableCategories.find(availableCategory => availableCategory.name.replace(" ", "").toLowerCase() === searchedCategory);
        if (!isCategoryExists) {
          this.router.navigateByUrl(defaultUrl);
        } else {
          this.sport = searchedSport;
          this.category = searchedCategory.charAt(0).toUpperCase() + searchedCategory.slice(1);
          this.getOnQueue();
          this.getData();
          this.signalRDataChanges();
        }
      }
    });
  }

  private signalRDataChanges() {
    this.signalRService.dataSubsValue.subscribe(res => {
      this.hideButton = false;
      this.hideJoinButton = false;
      this.getOnQueue();
      if(this.signalRService.data[0].sport === this.sport && this.signalRService.data[0].category === this.category && this.signalRService.data[0].city === this.city) {
        if(this.refreshDataSet.length > 0) {
          if(this.signalRService.data.length > this.refreshDataSet.length) {
            this.refresh = true;
            this.refreshDataSet = this.signalRService.data;
          } else {
            this.dataSet = this.signalRService.data;
          }
        }
        if(this.refreshDataSet.length === 0 ) {
          if (this.signalRService.data.length > this.dataSet.length) {
            this.refresh = true;
            this.refreshDataSet = this.signalRService.data;
          } else {
            this.dataSet = this.signalRService.data;
          }
        } 
      }
    }, err => {
      console.log(err);
    });
  }

  public refreshData() {
    this.dataSet = JSON.parse(JSON.stringify(this.refreshDataSet));
    this.refreshDataSet.length = 0;
    this.refresh = false;
  }

  public joinQueue(matchid) {
    const data = {matchID: matchid, userID: this.userId};
    this.matchService.addUserToMatch(data).subscribe(res => {
    }, err => {
      console.log(err);
    });
  }

  public cancelQueue(matchid) {
    const data = {matchID: matchid, userID: this.userId};
    this.matchService.cancel(data).subscribe(res => {
    }, err => {
      console.log(err);
    });
  }

  public lessThan(f, s) {
    return parseInt(f) <= parseInt(s);
  }

  private getData() {
    this.dataSet.length = 0;
    this.matchService.getAutoFilteredMatches(this.sport, this.category, this.city).subscribe(res => {
      this.dataSet = this.dataSet.concat(res);
    }, err => {
      console.log(err);
    });
  }

  private getOnQueue() {
    this.appliedMatches.length = 0;
    this.matchService.onqueue(this.userId).subscribe(res => {
      for(const agg of res) {
        this.appliedMatches.push(agg.matchID);
      }
    }, err => {
      console.log(err);
    });
  }

  public delete(id, index) {
    this.dataSet.splice(index, 1);
    this.matchService.deleteMatch(id).subscribe(res => {
    }, err => {
      console.log(err);
    })
  }
}
