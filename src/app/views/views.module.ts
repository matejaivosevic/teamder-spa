import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewsRoutingModule } from './views-routing.module';
import {CalendarModule} from 'primeng/calendar';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';   
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {CheckboxModule} from 'primeng/checkbox';
import {TabViewModule} from 'primeng/tabview';
import {RatingModule} from 'primeng/rating';
import {MatInputModule} from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';
import { MatSelectModule } from '@angular/material/select';
import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';
import { UserHomeComponent } from './user-home/user-home.component';
import { SportComponent } from './sport/sport.component';
import { CreateMatchFormComponent } from './create-match-form/create-match-form.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {TooltipModule} from 'primeng/tooltip';
import { RoomComponent } from './room/room.component';
import { ChatComponent } from './chat/chat.component';
import { AppModule } from '../app.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
  UserHomeComponent,
  SportComponent,
  CreateMatchFormComponent,
  UserProfileComponent,
  RoomComponent,
  ChatComponent
],
  imports: [
    CommonModule, 
    ViewsRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    MatNativeDateModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    AccordionModule,
    CheckboxModule,
    TabViewModule,
    RatingModule,
    TooltipModule,
    RouterModule
  ],
  exports :[
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    MatNativeDateModule,
    BsDropdownModule
  ],
})
export class ViewsModule { }
