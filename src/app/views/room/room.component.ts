import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatchService } from 'src/app/services/match.service';
import { SignalRService } from 'src/app/services/signal-r.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import * as signalR from '@aspnet/signalr';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

  public matchId;
  public currentMsgText= "";
  public match;
  public userList = [];
  public messages = [];
  public uploadForm: FormGroup;
  public myid = JSON.parse(localStorage.getItem('user')).userID;
  private hubConnection: signalR.HubConnection;

  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private matchService: MatchService,
    private signalRService: SignalRService) { 
      this.uploadForm = this.createFormGroupForUpload();
    }

  ngOnInit() {
    this.startChatConnection();
    this.matchId = this.route.snapshot.params.matchId;
    this.getMessages();
    this.signalRService.startConnection();
    this.signalRService.addQueueIncrementDataListener();
    this.getMatch();
    this.getMatchPlayers();
    this.signalRDataChanges();
  }

  public ChatStarter = () => {
    this.hubConnection.on(this.matchId + this.match.name + 'teamtramessagehubns', (data) => {
      this.messages.unshift(data);
      this.currentMsgText = "";
    });
  }

  private startChatConnection() {
    this.hubConnection = new signalR.HubConnectionBuilder()
                            .withUrl('https://localhost:5001/teamtramatchesqueuehub')
                            .build();

    this.hubConnection
    .start()
    .then(() => console.log('Chat activated.'))
    .catch(err => console.log('Error while starting chat: ' + err))  
  }

  private signalRDataChanges() {
    this.signalRService.dataSubsValue.subscribe(res => {
      let matchch = this.signalRService.data.filter(x => x.matchID == this.matchId)[0];
      if(matchch.queueNumber != this.match.queueNumber) {
        this.match.queueNumber = matchch.queueNumber;
        this.getMatchPlayers();
      }
    }, err => {
      console.log(err);
    });
  }

  private getMatch() {
    this.matchService.getMatchById(this.matchId).subscribe(res => {
      this.match = res;
      this.ChatStarter();
    }, err => {
      console.log(err);
    });
  }

  private getMatchPlayers() {
    this.matchService.getAllPlayersForMatch(this.matchId).subscribe(res => {
      this.userList.length = 0;
      this.userList = this.userList.concat(res);
    }, err => {
      console.log(err);
    });
  }

  private getMessages() {
    this.matchService.getMessages(this.matchId).subscribe(res => {
      this.messages = this.messages.concat(res);
    }, err => {
      console.log(err);
    });
  }

  createFormGroupForUpload(): FormGroup {
    return this.formBuilder.group({
      'files': [[]],
      'link': ['']
    });
  }

  sendMessage() {
    const data = {
      matchID: this.matchId,
      text: this.currentMsgText,
      sender: this.myid,
      senderPic: JSON.parse(localStorage.getItem('user')).profilePicturePath,
      matchName: this.match.name,
      time: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), new Date().getHours()
      , new Date().getMinutes(), new Date().getSeconds()).toISOString(),
    }

    this.matchService.sendTextMessage(data).subscribe(res => {
    }, err => {
      console.log(err);
    });

  }

  importFile(event) {
    const data = {
      matchID: this.matchId,
      text: "pic",
      sender: this.myid,
      senderPic: JSON.parse(localStorage.getItem('user')).profilePicturePath,
      matchName: this.match.name,
      time: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), new Date().getHours()
      , new Date().getMinutes(), new Date().getSeconds()).toISOString()
    }
    console.log(data);
    if (event.target.files.length == 0) {
       console.log("No file selected!");
       return
    }
      let file: File = event.target.files[0];
      const formData: FormData = new FormData();
      formData.append('file', file);
      
      this.matchService.sendMessageWithPicture(formData, data).subscribe(res => {
      }, err => { 
        console.log(err);
      });

  }

  public get files() {
    return this.uploadForm.get('files');
  }

  public get link() {
    return this.uploadForm.get('link');
  }

}
