import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import Utils from 'src/app/utils';

@Component({
  selector: 'app-update-user-form',
  templateUrl: './update-user-form.component.html',
  styleUrls: ['./update-user-form.component.scss']
})
export class UpdateUserFormComponent implements OnInit {
  currentUser;
  updateUserForm: FormGroup;

  playerCategories = ["Beginner", "Regular", "Proffesional"];

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) {
    this.updateUserForm = this.createUpdateUserForm();
   }

  birthYears = []

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.getUser();

    this.birthYears = Utils.getRangeOfYears();
  }

  public getUser() {
    this.userService.getUserByID(this.currentUser.userID).subscribe(res => {
      this.updateUserForm.patchValue({...res, password: ''});     
    }, err => {
      console.log(err);
    });
  }

  private createUpdateUserForm() {
    return this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      phoneNumber: [''],
      age: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      city: ['', Validators.compose([Validators.required])],
      category: ['']
    });
  }

  public submitForm() {
    console.log(this.updateUserForm.value);
    this.userService.updateUserData({...this.updateUserForm.value, userID: this.currentUser.userID}).subscribe(res => {
      this.router.navigateByUrl('user/profile')
    }), err => {
      console.log(err);
    };
  }
}
