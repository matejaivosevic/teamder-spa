import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserHomeComponent } from './user-home/user-home.component';
import { AuthTokenGuard } from '../authorization/auth-token.guard';
import { SportComponent } from './sport/sport.component';
import { CreateMatchFormComponent } from './create-match-form/create-match-form.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { RoomComponent } from './room/room.component';
import { ChatComponent } from './chat/chat.component';

const routes: Routes = [
  { path: '', 
      children: [
        {
          path: '',
          redirectTo: 'home', canActivate: [AuthTokenGuard]
        },
        { path: 'home', component: UserHomeComponent, canActivate: [AuthTokenGuard] },
        { path: 'sport/:sport/:mode', component: SportComponent, canActivate: [AuthTokenGuard] },
        { path: 'match-form/:sport/:mode', component: CreateMatchFormComponent, canActivate: [AuthTokenGuard] },
        { path: 'match-form/:sport/:mode/:id', component: CreateMatchFormComponent, canActivate: [AuthTokenGuard] },
        { path: 'profile', component: UserProfileComponent},
        { path: 'profile/:userident', component: UserProfileComponent},
        { path: 'room/:matchId', component: RoomComponent, canActivate: [AuthTokenGuard]},
        { path: 'chat', component: ChatComponent, canActivate: [AuthTokenGuard]}
      ]
 }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewsRoutingModule { }
