import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  public user;
  public userident;
  public backUser;
  public uploadForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute) { 
    this.uploadForm = this.createFormGroupForUpload();
  }
  
  currentYear = new Date().getFullYear();

  ngOnInit() {
    this.userident = this.route.snapshot.params.userident;
    if(this.userident) {
      this.getUserIdent();
    } else {
      this.user = JSON.parse(localStorage.getItem('user'));
      this.getUser();
    }
  }

  private getUserIdent() {
    this.userService.getUserByID(this.userident).subscribe(res => {
      this.backUser = res;
      this.user = res;
    }, err => {
      console.log(err);
    });
  }

  public getUser() {
    this.userService.getUserByID(this.user.userID).subscribe(res => {
      this.backUser = res;
    }, err => {
      console.log(err);
    });
  }

  createFormGroupForUpload(): FormGroup {
    return this.formBuilder.group({
      'files': [[]],
      'link': ['']
    });
  }

  importFile(event) {
    if (event.target.files.length == 0) {
       console.log("No file selected!");
       return
    }
      let file: File = event.target.files[0];
      const formData: FormData = new FormData();
      formData.append('file', file);
      this.userService.addProfilePicture(file.name, this.user.userID, formData).subscribe(res => {
        this.backUser = null;
        this.getUser();
      }, err => {
        console.log(err);
      });
    }

  public get files() {
    return this.uploadForm.get('files');
  }

  public get link() {
    return this.uploadForm.get('link');
  }


}
