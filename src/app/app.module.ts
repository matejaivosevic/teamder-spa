import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {MatInputModule} from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';
import { MatSelectModule } from '@angular/material/select';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';
import { LandingModule } from './landing-page/landing.module';
import { CommonModule  } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthorizationInterceptor } from './authorization/set-token-interceptor.service';
import { EmailConfirmationComponent } from './authorization/email-confirmation/email-confirmation.component';
import { SendPasswordChangeRequestComponent } from './authorization/send-password-change-request/send-password-change-request.component';
import { PasswordChangeRequestPageComponent } from './authorization/password-change-request-page/password-change-request-page.component';
import { AccountNotVerifiedComponent } from './authorization/account-not-verified/account-not-verified.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { UpdateUserFormComponent } from './views/update-user-form/update-user-form.component';
import { ViewsModule } from './views/views.module';
import { RouterModule } from '@angular/router';
import { LandingProfileComponent } from './views/landing-profile/landing-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    DefaultLayoutComponent,
    EmailConfirmationComponent,
    SendPasswordChangeRequestComponent,
    PasswordChangeRequestPageComponent,
    AccountNotVerifiedComponent,
    UpdateUserFormComponent,
    LandingProfileComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    LandingModule,
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    MatNativeDateModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports :[
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    MatNativeDateModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
