import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const CONFIG = require('./app-config.json');

const LOCAL_URL = CONFIG.CURRENT_URL;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public addProfilePicture(fileName: any, id: any, data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}file/add/${fileName}/${id}`, data);
  }

  public getUserByID(id: any): Observable<any> {
    return this.http.get(`${LOCAL_URL}useridentity/finduser/${id}`);
  }

  public updateUserData(user: any): Observable<any> {
    return this.http.put(`${LOCAL_URL}useridentity/update`, user);
  }
  public getid(): Observable<any> {
    return this.http.get(`${LOCAL_URL}useridentity/getid`);
  }
}
