import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  public data;
  public dataSubsValue = new Subject();
 
private hubConnection: signalR.HubConnection
 
  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
                            .withUrl('https://localhost:5001/teamtramatchesqueuehub')
                            .build();
 
    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err))
  }
 
  public addQueueIncrementDataListener = () => {
    this.hubConnection.on('teamtramatchesqueuehubsubscription', (data) => {
      this.data = data;
      this.dataSubsValue.next(data);
    });
  }
}
