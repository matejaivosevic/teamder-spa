import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
const CONFIG = require('./app-config.json');

const LOCAL_URL = CONFIG.CURRENT_URL;

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor(private http: HttpClient) { }

  public createMatch(data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}match/creatematch`, data);
  }

  public getAutoFilteredMatches(sport: any, category: any, city: any ): Observable<any> {
    return this.http.get(`${LOCAL_URL}match/autofilter?sport=${sport}&category=${category}&city=${city}`);
  }

  public getMatchById(id: any): Observable<any> {
    return this.http.get(`${LOCAL_URL}match/match/${id}`);
  }

  public updateMatch(data: any): Observable<any> {
    return this.http.put(`${LOCAL_URL}match/update`, data);
  }

  public addUserToMatch(data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}match/addtomatch`, data);
  }

  public cancel(data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}match/cancel`, data);
  }

  public onqueue(userid: any): Observable<any> {
    return this.http.get(`${LOCAL_URL}match/onqueue/${userid}`);
  }

  public getAllPlayersForMatch(id: any): Observable<any> {
    return this.http.get(`${LOCAL_URL}match/matchplayers/${id}`);
  }

  public deleteMatch(id: any): Observable<any> {
    return this.http.delete(`${LOCAL_URL}match/delete/${id}`);
  }

  public sendTextMessage(data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}match/sendtxt`, data);
  }

  public sendMessageWithPicture(file: any, data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}match/sendimg/${data.matchID}/${data.text}/${data.sender}/${data.time}/${data.matchName}`, file);
  }

  public getMessages(id: any): Observable<any> {
    return this.http.get(`${LOCAL_URL}match/getmessages/${id}`);
  }
}
