import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const CONFIG = require('./app-config.json');

const LOCAL_URL = CONFIG.CURRENT_URL;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}useridentity/authenticate`, data);
  }

  public verifyAccount(data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}useridentity/verifyaccount`, data);
  }

  public register(data: any): Observable<any> {
    return this.http.post(`${LOCAL_URL}useridentity/register`, data);
  }
}
