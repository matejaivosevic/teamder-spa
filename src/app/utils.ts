const currentYearOffset = 10;
const rangeOfYears = 60;

export default class Utils {
  static getRangeOfYears(): Array<String> {
    const currentYear = new Date().getFullYear();
    const years = [];
    for (let i = currentYear - currentYearOffset; i >= currentYear - (currentYearOffset + rangeOfYears); i--) {
        years.push(i.toString());
    }
    return years;
  }
}
