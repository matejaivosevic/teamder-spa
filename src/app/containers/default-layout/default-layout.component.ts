import {Component, OnInit } from '@angular/core';
import { navItems } from '../../_nav';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./layout-styles.scss']
})
export class DefaultLayoutComponent implements OnInit {

  public userpic;

  constructor(private userService: UserService,
    private router: Router) {
  }
  ngOnInit() {
    this.userService.getid().subscribe(res => {
      this.userpic = res.claims[7].value;
    }, err => {
      console.log(err);
    });
  }

  public navtochat() {
    this.router.navigate(["/user/chat"]);    
  }

  public sidebarMinimized = false;
  public navItems = navItems;

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  public logout() {
    localStorage.clear();
    this.router.navigateByUrl('');
  }
}
