import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Utils from 'src/app/utils';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  public emailRegex: RegExp = new RegExp('^[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}$');
  public registerForm: FormGroup;
  public ages = [1,2,3,4,5];

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router) {
    this.registerForm = this.createRegisterForm();
  }

  birthYears = []

  ngOnInit() {
    this.birthYears = Utils.getRangeOfYears();
  }

  private createRegisterForm(): FormGroup {
    return this.formBuilder.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'email': ['', Validators.compose([Validators.required, Validators.email, Validators.pattern(this.emailRegex)])],
      'lastName': ['', Validators.compose([Validators.required])],
      'username': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      'confirmPassword': ['', Validators.compose([Validators.required])],
      'age': ['', Validators.compose([Validators.required])],
      'city': ['', Validators.compose([Validators.required])]
    });
  }

  public submitForm() {
    console.log(this.registerForm.value);
    this.authService.register(this.registerForm.value).subscribe(res => {
      this.router.navigateByUrl('/accountnotverified');
    }, err => {
      console.log(err);
    });
  }

  public addAges(){
    for (let index = 12; index <= 90; index++) {
      this.ages[index-12] = index; 
    }
  }
  
  public get firstName() {
    return this.registerForm.get('firstName');
  }
  
  public get email() {
    return this.registerForm.get('email');
  }

  public get lastName() {
    return this.registerForm.get('lastName');
  }

  public get username() {
    return this.registerForm.get('username');
  }

  public get password() {
    return this.registerForm.get('password');
  }

  public get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  public get age() {
    return this.registerForm.get('age');
  }

  public get city() {
    return this.registerForm.get('city');
  }
}
