import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordChangeRequestPageComponent } from './password-change-request-page.component';

describe('PasswordChangeRequestPageComponent', () => {
  let component: PasswordChangeRequestPageComponent;
  let fixture: ComponentFixture<PasswordChangeRequestPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordChangeRequestPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordChangeRequestPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
