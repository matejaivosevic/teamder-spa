import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-not-verified',
  templateUrl: './account-not-verified.component.html',
  styleUrls: ['./account-not-verified.component.scss']
})
export class AccountNotVerifiedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
