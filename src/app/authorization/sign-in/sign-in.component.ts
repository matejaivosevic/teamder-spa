import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  public loginForm: FormGroup;
  public emailRegex: RegExp = new RegExp('^[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}$');

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router) {
    this.loginForm = this.createLoginForm();
   }

   errorMessage;
   authenticationStatus;

  ngOnInit() {
  }

  public submitForm() {
    this.authenticationStatus = "Wait...";
    this.authService.login(this.loginForm.value).subscribe(data => {
      if(data.user.isVerified === 'True') {
        localStorage.setItem('user', JSON.stringify(data.user));
        localStorage.setItem('token', JSON.stringify(data.auth_token));
        if(data.user.role === 'Player') {
          this.router.navigateByUrl('/user');
        }
      } else {
        this.router.navigateByUrl('/accountnotverified');
      }
      this.errorMessage = "";
    }, err => {
      this.errorMessage = err.error.message;
      this.authenticationStatus = "";
    });
  }

  private createLoginForm() {
    return this.formBuilder.group({
      'email' : ['', Validators.compose([Validators.required, Validators.email, Validators.pattern(this.emailRegex)])],
      'password' : ['', Validators.compose([Validators.required])]
    });
  }

  public get email() {
    return this.loginForm.get('email');
  }

  public get password() {
    return this.loginForm.get('password');
  }
  
}
