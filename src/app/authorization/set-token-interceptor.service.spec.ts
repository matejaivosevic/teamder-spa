import { TestBed } from '@angular/core/testing';

import { SetTokenInterceptorService } from './set-token-interceptor.service';

describe('SetTokenInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetTokenInterceptorService = TestBed.get(SetTokenInterceptorService);
    expect(service).toBeTruthy();
  });
});
