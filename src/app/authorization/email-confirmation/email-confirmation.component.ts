import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.scss']
})
export class EmailConfirmationComponent implements OnInit {

  public userId;
  public token;
  public validToken;

  constructor(private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit() {
    this.userId = this.route.snapshot.params.userId;
    this.token = this.route.snapshot.params.token;
    this.verifyAccount();
  }

  private verifyAccount() {
    this.authService.verifyAccount({userID: this.userId, token: this.token}).subscribe(data => {
      if(data.response === 'success') {
        this.validToken = 'valid';
      } else {
        this.validToken = 'invalid';
      }
    }, err => {
      console.log(err);
    });
  }

}
