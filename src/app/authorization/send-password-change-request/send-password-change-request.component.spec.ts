import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendPasswordChangeRequestComponent } from './send-password-change-request.component';

describe('SendPasswordChangeRequestComponent', () => {
  let component: SendPasswordChangeRequestComponent;
  let fixture: ComponentFixture<SendPasswordChangeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendPasswordChangeRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendPasswordChangeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
