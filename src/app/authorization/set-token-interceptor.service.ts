import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const token = JSON.parse(localStorage.getItem('token'));
      if (token) {
          req = req.clone({
              setHeaders: {
                  Authorization: `Bearer ${token}`
              }
          });
      }
      return next.handle(req);
  }
}
