import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './containers';
import { LandingComponent } from './landing-page/landing/landing.component';
import { SignInComponent } from './authorization/sign-in/sign-in.component';
import { AuthTokenGuard } from './authorization/auth-token.guard';
import { SignUpModule } from './authorization/sign-up/sign-up.module';
import { SignUpComponent } from './authorization/sign-up/sign-up.component';
import { EmailConfirmationComponent } from './authorization/email-confirmation/email-confirmation.component';
import { AccountNotVerifiedComponent } from './authorization/account-not-verified/account-not-verified.component';
import { UpdateUserFormComponent } from './views/update-user-form/update-user-form.component';

const routes: Routes = [  
  {
    path: '',
    component: LandingComponent,
    children: [
      {
        path: 'landing',
        loadChildren: () => import('./landing-page/landing.module').then(m => m.LandingModule)
      },
    ]
  },
  {
    path: 'user',
    component: DefaultLayoutComponent,

    children: [
      {
        path: '',
        loadChildren: () => import('./views/views.module').then(m => m.ViewsModule),
        //canActivate: [AuthTokenGuard]
      },
      {
        path: 'update',
        component: UpdateUserFormComponent
      },
    ]
  },
  {
    path: 'login',
    component: SignInComponent,
    children: [
      {
        path: '',
        loadChildren: './authorization/sign-in/sign-in.module#SignInModule'
      },
    ]
  },
  {
    path: 'register',
    component: SignUpComponent,
    children: [
      {
        path: '',
        loadChildren: './authorization/sign-up/sign-up.module#SignUpModule'
      },
    ]
  },
  {
    path: 'accountconfirmationpage/:userId/:token',
    component: EmailConfirmationComponent
  },
  {
    path: 'accountnotverified',
    component: AccountNotVerifiedComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
