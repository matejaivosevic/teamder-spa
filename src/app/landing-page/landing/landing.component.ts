import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  userLogged = false;

  constructor() { }

  ngOnInit() {
    if (localStorage.getItem('user')){
      this.userLogged = true
      console.log("User logged");
    } else {
      this.userLogged = false;
      console.log("User not logged");
    }
  }

}
