import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import { LandingHomeComponent } from './landing-home/landing-home.component';
import { LandingRoutingModule } from './landing-routing.module';
import { LandingMenuComponent } from './landing-menu/landing-menu.component';
import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';
import { TestDeleteComponent } from './test-delete/test-delete.component';
import { SignInModule } from '../authorization/sign-in/sign-in.module';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    LandingHomeComponent,
    LandingMenuComponent,
    LandingComponent,
    TestDeleteComponent
  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    HttpClientModule,
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
    SignInModule
  ]
})
export class LandingModule { }
