import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingHomeComponent } from './landing-home/landing-home.component';
import { TestDeleteComponent } from './test-delete/test-delete.component';

const routes: Routes = [
  { 
    path: '', 
    children: [
      { path: '', component: LandingHomeComponent },
      { path: 'hom', component: TestDeleteComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
